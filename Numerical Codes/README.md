# Daten codieren Aufgaben

## aufgaben zu numerischen Codes

### 1. Ergänzen sie die Tabelle. Studieren sie anschliessend ihre fertig ausgefüllte Tabelle, insbesondere die Kolonnen mit den Binärwerten. Was stellen sie fest?

|DEC|HEX|BIN 2<sup>3</sup>|BIN 2<sup>2</sup>|BIN 2<sup>1</sup>|BIN 2<sup>0</sup>|
|---|---|---|---|---|---|
| 0 | 0 | 0 | 0 | 0 | 0 |
| 1 | 1 | 0 | 0 | 0 | 1 |
| 2 | 2 | 0 | 0 | 1 | 0 |
| 3 | 3 | 0 | 0 | 1 | 1 |
| 4 | 4 | 0 | 1 | 0 | 0 |
| 5 | 5 | 0 | 1 | 0 | 1 |
| 6 | 6 | 0 | 1 | 1 | 0 |
| 7 | 7 | 0 | 1 | 1 | 1 |
| 8 | 8 | 1 | 0 | 0 | 0 |
| 9 | 9 | 1 | 0 | 0 | 1 |
| 10| A | 1 | 0 | 1 | 0 |
| 11| B | 1 | 0 | 1 | 1 |
| 12| C | 1 | 1 | 0 | 0 |
| 13| D | 1 | 1 | 0 | 1 |
| 14| E | 1 | 1 | 1 | 0 |
| 15| F | 1 | 1 | 1 | 1 |

### 2. Wandeln sie die folgende Dezimalzahl ohne Taschenrechner in die entsprechende Binärzahl um: **911**

|512|256|128|64 |32 |16 |8  |4  |2  |1  |
|---|---|---|---|---|---|---|---|---|---|
| 1 | 1 | 1 | 0 | 0 | 0 | 1 | 1 | 1 | 1 |

911 - 512 = 399

399 - 256 = 143

143 - 128 = 15 -> 1111

### 3. Wandeln sie die folgende Binärzahl ohne Taschenrechner in die entsprechende Dezimalzahl um: **1011 0110**

|512|256|128|64 |32 |16 |8  |4  |2  |1  |
|---|---|---|---|---|---|---|---|---|---|
| 0 | 0 | 1 | 0 | 1 | 1 | 0 | 1 | 1 | 0 |

2 + 4 + 16 + 32 + 128 = <u>**182**</u>

### 4. Wandeln sie die folgende Binärzahl ohne Taschenrechner in die entsprechende Hexadezimalzahl um: **1110 0010 1010 0101**

- 1110 -> E
- 0010 -> 2
- 1010 -> A
- 0101 -> 5

Lösung: <u>**E2 A5**</u> oder <u>**0xE2A5**</u>

### 5. Was ergibt die Addition der beiden binären Zahlen 1101 1001 und 0111 0101? Beachten sie, dass für das Resultat ebenfalls nur 8 Binärstellen zur Verfügung stehen.

![Bild zu Aufgabe 5](./img/signal-2024-05-13-090741_002.png)

Das Resultat ist grösser als 255 (wenn nicht zweierkomplement), darum ist das Endergebnis kleiner, wenn nur 8 bits benutzt wird. Das Extra bit wird als Datenüberlauf betrachtet und wird weggeworfen in meisten Betriebssystemen

### 6. Was könnten die beiden folgenden binären Wert für eine Bedeutung haben?

#### a. 1100 0000.1010 1000.0100 1100.1101 0011

- 1100 0000 -> 192
- 1010 1000 -> 168
- 0100 1100 -> 76
- 1101 0011 -> 211

192.168.76.211 

Es get um eine <u>**Ip Adresse**</u>

#### b. 1011 1110-1000 0011-1000 0101-1101 0101-1110 0100-1111 1110

- 1011 1110 -> BE
- 1000 0011 -> 83
- 1000 0101 -> 85
- 1101 0101 -> D5
- 1110 0100 -> E4
- 1111 1110 -> FE

BE-83-85-D5-E4-FE

Es get um eine <u>**MAC Adresse**</u>

### 7. Was könnte die folgende in einem Bash-Script entdeckte Zeile für eine Bedeutung haben? Hinweis: Hier handelt es sich um das oktale Zahlensystem. **chmod 751 CreateWeeklyReport**

|r - read|w - write|x - execute|
|---|---|---|
| 4 | 2 | 1 |

- rwx -> 4 + 2 + 1 = 7
- r-x -> 4 + 1 = 5
- --x -> 1

mit chmod kann ich die berechrigung einem Ordner oder in diesem Fall einer Datei verwalten. Mit diesem Befehl sagen wir, dass der User, der die Datei erstellt hat, hat alle berechtigung (read, write und executable -> rwx), die Gruppen, die zu User gehören, können nur lesen und schreiben (r-x) und der rest kann die Datei nur ausführen (--x)

### 8. Dimensionieren sie für den Matterhorn-Express, wo insgesamt 107 Gondeln die Touristen von Zermatt auf den Trockenen-Steg befördern, die Codebreite des Binärcodes für die Kabinenzählung.

mit 7 bits würde es die Zahl 107 passen.

2<sup>7</sup> = 128

128 > 107

### 9. Sie untersuchen einen Arbeitsspeicher mit 12-Bit-Adress- bzw. 16-Bit-Datenbus. Welche Speicherkapazität in kiB besitzt dieser? (Hinweis: 1kiB=1024B)

2<sup>12</sup> * 2<sup>16</sup> = 268'435'456 bits

268'435'456 / 8 / 10<sup>3</sup> = 33'554.432 oder 33'554 kiB

### 10. Zwei Geräte sind mit einer seriellen Leitung und zusätzlichem Taktsignal verbunden. Das Taktsignal beträgt 1MHz.

#### a. Wie viele Bytes können damit pro Sekunde übertragen werden?

In Theorie, könnte man 1Mbps (1 Millionen Bits pro Sekunde) übertragen

#### b. Wie viele Bytes pro Sekunde könnten übertragen werden, wenn die Verbindung der beiden Geräte nicht seriell, sondern 8 Bit-parallel wäre?

Mit gleichen Takt, könnte die Parallel verbindung 8Mbps übertragen

### 11.

#### a. Nennen sie kleinster und grösster Binärwert bzw. Dezimaläquivalent im Falle von unsigned bzw. Vorzeichenlos.

Grösste Wert: **255**

Kleinste Wert: **0**

#### b. Nennen sie kleinster und grösster Binärwert bzw. Dezimaläquivalent im Falle von signed bzw. Vorzeichenbehaftet.

Grösste Wert: **127**

Kleinste Wert: **-128**

#### c. Wandeln sie die Dezimalzahl +83 in einen vorzeichenbehafteten Binärwert um. (signed)

**0101 0011**

83 - 64 = 19
19 - 16 = 3 -> 0011

#### d. Wandeln sie die Dezimalzahl -83 in einen vorzeichenbehafteten Binärwert um. Signed mit 2er-Komplement:

##### erster Schritt: alle bits umkehren

| 0 | 1 | 0 | 1 | 0 | 0 | 1 | 1 |
|---|---|---|---|---|---|---|---|
| 1 | 0 | 1 | 0 | 1 | 1 | 0 | 0 |

**1010 1100**

##### zweiter Schritt: 1 bit dazu geben

1010 1100 + 1 bit = **1010 1101**

#### e. Addieren sie die beiden erhaltenen Binärwerte zusammen. Es sollte 0 ergeben!

![](./img/signal-2024-05-13-101801_002.jpeg)

#### f. Wandeln sie die Dezimalzahl 0 in einen vorzeichenbehafteten Binärwert um. (signed). Hat ihre vorangegangene Addition auch diesen Binärwert ergeben?

0 -> 0000 0000

Ja, es hat

#### g. Warum können sie bei der gegebenen Datenbusbreite von 1 Byte die Dezimalzahl +150 nicht in einen vorzeichenbehafteten Binärwert umwandeln?

Da es unklar ist, ob wir später den wert lesen, das es ist 150 oder -106 ist. die Zweier komplement muss anders angezeigt werden

### 12.

Man kann den Wissenschaftlichen Notation abspeichern. das heisst, die ersten hälfte bits speichern den Zahlen wert, das mit Zehn hoch multipliziert wird und die anderen Bits die hochzahl für Zehn.

3333333 * 10^-1000

3333333 die linke seite von bit und -1000 als zweier komplement die andere hälfte von 16 oder 32 bit Zahl

### 13. Erstellen sie die Wahrheitstabellen für die folgenden Funktionen:

#### Logisch UND/AND

| A | B |Out|
|---|---|---|
| 0 | 0 | 0 |
| 1 | 0 | 0 |
| 0 | 1 | 0 |
| 1 | 1 | 1 |

#### Logisch ODER/OR

| A | B |Out|
|---|---|---|
| 0 | 0 | 0 |
| 1 | 0 | 1 |
| 0 | 1 | 1 |
| 1 | 1 | 1 |

#### Logisch NICHT/NOT

| In|Out|
|---|---|
| 0 | 1 |
| 1 | 0 |

#### Logisch EXOR

| A | B |Out|
|---|---|---|
| 0 | 0 | 0 |
| 1 | 0 | 1 |
| 0 | 1 | 1 |
| 1 | 1 | 0 |

### 14. Eine in der Computertechnik wichtige mathematische Funktion ist die Restwert- oder Modulo-Funktion mit dem in z.B. Java und C verwendeten Operationszeichen %. Versuchen sie nun die folgende Berechnungen auszuführen. Was stellen sie fest?

1. 11 % 2 = 1
2. 10 % 2 = 0
3. 10 % 3 = 1
4. 10 % 5 = 0
5. 10 % 9 = 1

Mit Modulo, kann ich den Rest Wert erhalten, der bei normalen dividierung ignoriert wird (Wenn die Ausrechnung ohne Dezimal werten gerechnet wird)



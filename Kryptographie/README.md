# KRYPTOGRAFIE AUFGABEN

## SYMMETRISCHE VERSCHLÜSSELUNGSVERFAHREN

### 1) Cryptool-Applikation installieren

![](./img/Screenshot%202024-06-30%20115145.png)

### 2) Rotationschiffre

Input: 

```text
GHU DQJULII HUIROJW CXU WHHCHLW GLH ZXHUIHO VLQG JHIDOOHQ LFK NDP VDK XQG VLHJWH WHLOH XQG KHUUVFKH
```

Resultat: 
```text
DER ANGRIFF ERFOLGT ZUR TEEZEIT DIE WUERFEL SIND GEFALLEN ICH KAM SAH UND SIEGTE TEILE UND HERRSCHE
```

### 3) Vigenèreverschlüsselung

Input: BEEF

Key: AFFE

Result: BJJJ

---

Input: WRKXQT

Key: SECRET

decrypt: ENIGM

### 4) Vigenèrecodeanalyse

Input: 

```text
USP JHYRH ZZB GTV CJ WQK OCLGQVFQK GAYKGVFGX NS ISBVB MYBC MWCC NS JOEVB GTV KRQFV AGK XCUSP VFLVBLLBE ESSEILUBCLBXZU SENSWFGVRCES SER CZBCE ILUOLBPYISL CCSZG VZJ
```

key: ROY

Result: 

```text
DER STAAT BIN ICH ES IST AEUSSERST SCHWIERIG ZU REDEN OHNE VIEL ZU SAGEN ICH MACHE MIT JEDER ERNENNUNG NEUNUNDNEUNZIG UNZUFRIEDENE UND EINEN UNDANKBAREN LOUIS XIV
```

Der grosse schlüssel konnte nicht von der Vigenère-Analysetools nicht gefunden werden

### 5) XOR-Stromchiffre

4711 in hex: 1267

![](./img/Screenshot%202024-06-30%20120317.png)

### 6) AES (Advanced Encryption Standard)

### 7) Wie sicher ist mein Passwort?

![](./img/Screenshot%202024-06-30%20120957.png)

## ASYMMETRISCHE VERSCHLÜSSELUNGSVERFAHREN

### 1)

![](./img/Screenshot%202024-06-30%20121254.png)

### 2) RSA-Verschlüsselung

![](./img/Screenshot%202024-06-30%20122043.png)

### 3) 

### 4)

![](./img/Screenshot%202024-06-30%20122455.png)

### 5)

### 6)

### 7)

Wenn es keine änderung hat, wird die signatur validierung korrekt, wenn ich aber etwas verändere, ist die signatur anders und es wird nicht validiert

### 8)

#### A)

#### B)

#### C)

#### D) 

MD2 Hash 'original.txt' = `F9 71 51 B7 4F 63 3B C8 F3 9D 74 5A 11 C6 74 1A`
MD2 Hash 'fake.txt'     = `C2 24 7D 34 89 F2 58 C4 39 09 D7 4F AB 36 EC F9`
MD2 Hash 'backup.txt'   = `F9 71 51 B7 4F 63 3B C8 F3 9D 74 5A 11 C6 74 1A`

#### E)

#### F)

#### G)

![](./img/Screenshot%202024-06-30%20123923.png)

#### H)

## DIE SCHLÜSSELVERWALTUNG

### A) Wie kann ich einen Public-Key verifizieren?

Es braucht ein Vertrauensnetz oder von offiziellen Organisationen veröffent, damit man ein Public-schlüssel verifizeiren kann.

### B) Was versteht man unter Public Key Infrastruktur (PKI)?

Es ist ein System, das digitale Zertifikate austellt, verteilt und prüft

### C) Was bedeutet Certification-Authority (CA) und was Trust-Center (TC)?

Es sind Organisationen, die den X.509 digitale Zertifikate erstellen, verteilen und verifizieren können. sie veröffentlichen public keys und sind zuständig von archivierung und verwaltung von abgelaufenen Zertifikate

## SICHERES INTERNET UND ZERTIFIKATE

### 1) Wer hat das Zertifikat für die Bankwebseite www.ubs.com ausgestellt und wie lange ist es gültig?

DigiCert Inc hat das Zertifikat ausgestellt bis 12. Dec. 2024

![](./img/Screenshot%202024-06-30%20125155.png)

### 2) Wer hat das Zertifikat für die für die Schulwebseite www.tbz.ch ausgestellt und wie lange ist es gültig?

Let's Encrypt hat das Zertifikat ausgestellt bis 31. Aug. 2024

![](./img/Screenshot%202024-06-30%20125306.png)

### 3) Wer hat das Zertifikat für die für die Webseite www.example.ch ausgestellt und wie lange ist es gültig?

Sie haben keine Certificate und die Verbindung ist nicht gesichert

![](./img/Screenshot%202024-06-30%20125333.png)

### 4) Wählen sie irgendeine Applikation aus, die auf ihrem PC installiert ist. Stellen sie sich nun vor, sie müssten diese von Hand aktualisieren oder aus Kompatibilitätsgründen auf eine frühere Version zurückstufen. Wo finden sie aktuelle und frühere Versionen ihrer Software und wie wird sichergestellt, dass die dort angebotene SW-Version auch wirklich echt ist bzw. vom SW-Entwickler stammt?

Konnte keine software, das ich installiert habe, ein weg, um alte Versionen zu herunterladen und sie auch verifizieren

### 5) Erstellen sie eine virtuelle Linux-Maschine mit z.B. VirtualBox und Ubuntu. Richten sie nun auf ihrem WIN-PC eine Remoteverbindung via ssh zu ihrem Linux-PC ein. Überprüfen sie die Verbindung. Wäre auch eine graphische Anbindung möglich?

Im AWS Modul, musste ich ein public key in dem Ubuntu Instanz weitergeben und mit meinem Privat schlüssel konnte ich zu dem VM verbinden. Man kann auch ein passwort hinterlegen. Verbindung durch ein GUI wäre auch möglich

### 6)

Bei einer HTTPS verbindung, muss der Client und der Server eine Verbindung erstellen (Hand-Shake), nach der Verbindung kann es gesicherten Nachrichten schicken, wobei wireshark sie nicht lesen kann. Das ist dafür gedacht, das keine dritt-Person kann die einzelnen Nachrichten lesen

### 7) Öffnen sie die beiden folgenden Webseiten und achten sie auf die Unterschiede in der Webadresszeile. Was stellen sie bezüglich Protokoll und Zertifikat fest?

Es gibt verschiedene starke (Qualikative) Zertifikate, die man bekommen kann. Let's Encrypt ist nicht so stark, wie das von ZKB

### 8) Was genau wird hier zu welchen Konditionen angeboten?

CaCert ist kostenpflichtig und braucht extra Informationen, um ein Zertifikat zu erhalten

### 9) Folgende TLS Zertifikatsarten werden unterschieden: Domain Validated, Organization Validated und Extended Validation. Sie möchten einen Webshop betreiben, wo mit Kreditkarte bezahlt werden kann. Welcher Zertifikatstyp ist der richtige?

Das beste Zertifikatstyp ist der "Extended Validation" Certifikat, denn es ist am meisten "trusted" zertifikat. Es wird auch benötigt, wenn man sensitiven Daten Speicert wie die Kreditkarten nummern.

### 10) Was ist der Unterschied zwischen OpenPGP und X.509?

Die sind verschiedene Zertifikate und sind nicht kompatibel.

### 11) Wie ist der Ablauf beim Protokoll TLS? Wo genau kommen die Zertifikate ins Spiel?

Im OSI-Schicht 3 und 4 kommen sie ins spiel

### 12) Was bedeutet S/MIME?

Secure Multipurpose Internet Mail Extension. Standard für verschlüsselung von E-Mails

### 13) Aus gesetzlichen Gründen sind sie verpflichtet, den gesamten geschäftlichen EMail-Verkehr zu archivieren, auch den verschlüsselten. Was ist das Problem dabei und wie könnte man dies lösen?

Der Schüssel zur E-Mail kann verloren gehen und man kann nicht mehr die Daten von der Email lesen

### 14) 

## PGP und OpenPGP

### 1)

### 2)

#### A) 

#### B) 

#### C) 

#### D) 

#### E) 

#### F) 

#### G) 

### 20) Fremden Public-Key verifizieren: Wie können sie die Authentizität des Ausstellerschlüssels überprüfen? Stammt dieser Public-Key auch wirklich von der Person, von der ich dies annehme?

Man kann es nur mit jemanden absprechen, das es wirklich von dir gekommen ist (Web of Trust)

### 21)  Frage zum OpenPGP-Schlüssel: Woraus besteht bzw. woran erkennt man diesen?

wenn es in der Datei das steht: 

`-----BEGIN PGP PUBLIC KEY BLOCK-----`

### 22) Was sind die Unterschiede zwischen den beiden Schlüsselvarianten und was hat das mit S/MIME zu tun?

Man muss viel mehr auf dem Certificate aufschreiben, nicht nur eine E-Mail addresse. Man muss es auch an einem CA abschicken und man kann in einem Text Editor es nicht lesen

### 23) 

# Daten Kompremierung Aufgaben

## Aufgaben zu Daten komprimierung

### Kennen sie noch andere Gebiete in der IT, wo Baumstrukturen zur Anwendung kommen? Was unterscheidet einen binären Baum von einem nicht binären Baum?

Baum strukturen werden in den Dateisystemen von Betriebsystemen benutzt wie Windows, Linux oder MacOS. Es wird auch für Parsers benutzt, wie bei XML-Parser. Python Interperter wird auch Baum Strukur basiert.

Der Unterschied zu einem Binärenbaumstruktur ist, dass es nur maximum zwei Äste haben kann. wobei bei einer normalen Baumstruktur nicht der Fall ist.

### Demo mit Wort "Internet"

![](./img/signal-2024-05-27-092608.jpeg)

### Wie könnte die Komprimierung ausschauen, wenn es sich anstatt um ein Schwarz/Weissbild, um ein Farbbild handelt?

Bei einer Farbenbild muss noch die Farbe selbst gespeichert. also: 3 mal Grün und 6 mal Blau usw. die Farben könnte als hex gespeichert werden. bei indexierten Farben, kann auch eine Zahl gespeichert werden. Bei einer einfarbigen Bild wird nur die Farbe und anzahl gespeichert.

### Zeichnen sie die Grafik auf. Was stellt sie dar?

3 bit, 8 pixel breite, Schwarz-Weiss

010 100 011 110 | 2 4 3 6

010 010 010 010 | 2 2 2 2

010 010 010 010 | 2 2 2 2

010 110 010 110 | 2 6 2 6

010 010 010 010 | 2 2 2 2

010 010 010 010 | 2 2 2 2

001             | 1

#### Resultat (Buchstabe A):

![](./img/Image-encoding.png)

### LZW-Verfahren

#### A) Erstellen sie die LZW-Codierung für das Wort «ANANAS» und überprüfen sie mit der Dekodierung ihr Resultat.

![](./img/signal-2024-05-27-110242.jpeg)

Ergebnis: "AN256AS"

#### B) Versuchen sie den erhaltenen LZW-Code «ERDBE<256>KL<260>» zu dekomprimieren.

![](./img/signal-2024-05-27-113207.jpeg)

Ergebnis: "ERDBEERKLEE"
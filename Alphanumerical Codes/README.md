# Daten codieren Aufgaben

## Aufgaben zu alphanumerischen Codes

### 1. Beantworten sie nun die folgenden Fragen:

#### a. Welche der Dateien ist nun ASCII-codiert, welche UTF-8 und welche UTF-16 BE-BOM?

- Textsample1 = ASCII
- Textsample2 = UTF-8
- Textsample3 = UTF-16 BE-BOM

#### b. Alle drei Dateien enthalten denselben Text. Aus wie vielen Zeichen besteht dieser?

Sie bestehen aus 68 Zeichen

#### c. Was sind die jeweiligen Dateigrössen? (Beachten sie, dass unter Grösse auf Datenträger jeweils 0 Bytes angegeben wird. Dies darum, weil beim Windows-Dateisystem NTFS kleine Dateien direkt in die MFT (Master File Table) geschrieben werden.) Wie erklären sie sich die Unterschiede?

- Textsample1 = 68 bytes
- Textsample2 = 71 bytes
- Textsample3 = 138 bytes

Bei ASCII sind es immer 8 bits oder 1 byte. In UTF-8 kann ein Zeichen ausserhalb ASCII zwei oder mehr Bytes brauchen. bei UTF-16 BE-BOM ist jedes Zeichen 2 bytes lang und bei UTF-16 die ersten zwei bytes geben an, welche Ordnung sind die Bytes geordnet, ob big endien oder little endien. darum ist die grösse von Textsample3 nicht gleich doppelt Textsample1.

#### d. Bei den weiteren Fragen interessieren uns nur noch die ASCII- und die UTF-8-Datei: Bekanntlich ist UTF-8 in den ersten 128 Zeichen deckungsgleich mit ASCII. Untersuchen sie nun die beiden HEX-Dumps und geben sie an, welche Zeichen unterschiedlich codiert sind. Ein kleiner Tipp: Es sind deren zwei.

das ä und € zeichen

#### e. Was bedeuten die beiden Ausdrücke, denen wir z.B. bei UTF-16 begegnen: Big-Endian (BE), Little-Endian (LE)?

BE und LE bezeichen, wie die bytes aufgeordnet sind. bei BE ist das höhere bit ganz links, üblich wie bei bits. Mit LE nimmt man den grössten bit und gibt in ganz rechts, also umgekehr zu BE aber die bytes selbst verwecheln wir nicht. Es geht mehr um die platzierung der bytes.

#### f. Im Notepad++ kann man unter dem Menüpunkt Codierung von ASCII zu UTF umschalten. Spielen sie damit etwas herum und notieren sie sich, was in der Darstellung jeweils ändert.

Die Zeichen, die in ASCII nicht geben, werden verwirrend dargestellt. die anderen Zeichen sehen gleich aus

#### g. Der UTF-8-Code kann je nach Zeichen ein, zwei, drei oder vier Byte lang sein. Wie kann der Textreader erkennen, wann ein UTF-8 Zeichencode beginnt und wann er endet? Untersuchen sie dies anhand der beiden Textsamples und lesen sie in z.B. Wikipedia die entsprechende Theorie zu UTF-8 durch. Tipp: Startbyte und Folgebyte.

Wenn es um ASCII Code geht, wird es gleich dargestellt, wenn es aber ausserhalb der ASCII ist, wird es Startbyte und Folgebyte benutzen. Das Startbyte kann so aussehen: 110xxxxx. die einer geben an, wie viele Bytes dieser Zeichen hat. die Null trennt die nullen mit anderen bits. 10xxxxxx ist das Folgebyte und wird immer mit 10 anfangen, da weiss der Reader, dass es das nächste byte zu dem Zeichen gehört. In Theorie kann der Startbyte so aussehen: 11110xxx mit 4 bytes länge.

### 2.

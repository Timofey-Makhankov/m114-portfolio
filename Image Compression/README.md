# BILDER CODIEREN & KOMPRIMIEREN AUFGABEN

## Aufgaben zu Bilder codieren:

### 1) Bestimmen sie die Farben für die folgenden RGB-Farbcodes (in DEZ und HEX).

- RGB(255, 255, 255) = weiss
- RGB(0, 0, 0) = schwarz
- RGB(252, 178, 91) = gold-gelb
- #FF0000 = rot
- #00FF00 = grün
- #0000FF = blau
- #FFFF00 = gelb
- #00FFFF = cyan
- #FF00FF = magenta
- #000000 = schwarz
- #FFFFFF = weiss
- #00BC00 = wald-grün

### 2) Bestimmen sie die Farben für die folgenden prozentualen CMYK-Angaben.

- cmyk(0%, 100%, 100%, 0%) = rot
- cmyk(100%, 0%, 100%, 0%) = grün
- cmyk(100%, 100%, 0%, 0%) = blau
- cmyk(0%, 0%, 100%, 0%) = gelb
- cmyk(100%, 0%, 0%, 0%) = cyan
- cmyk(0%, 100%, 0%, 0%) = magenta
- cmyk(100%, 100%, 100%, 0%) = schwarz
- cmyk(0%, 0%, 0%, 100%) = schwarz
- cmyk(0%, 0%, 0%, 0%) = weiss
- cmyk(0%, 46%, 38%, 22%) = salmon (Lachsfarben)

### 3) Berechnen sie den theoretischen Speicherbedarf in Bit und in Byte eines unkomprimierten RGB-Bildes mit der Grösse 640 x 480 und 8Bit Auflösung pro Farbkanal.

1 pixel = 24 bit (3 kanal * 8 bit auflösung)
640 x 480 = 307'200 pixel

307000 * 24 = 7'372'800 bits oder 921'600 bytes

### 4) Für welche Bildformate werden sie sich entscheiden? Begründen sie!

für den hintergrund, würde ich webp oder jpeg nutzen, da ich das bild sehr klein halten kann und wenig speicher brauchen und wenig bandwidth, da es mit background-repeat: repeat gestyled wird. Für das Logo würde ich ein SVG nutzen, das egal nach grösse sind die Linien sehr gerade und glatt, wobei bei einer Raster-bild sehr eckig machen könnte.

### 5) Sie haben ein 30-Zoll-Display (Diagonale) im Format 16:10 und 100ppi erworben. Was ist die Pixelauflösung horizontal und vertikal?

30 * 100 = 3000 pixel diagonally

3000 = √(a^2 + b^2)

2544 x 1589 auflösung

### 6) Sie drucken ein quadratisches Foto mit einer Kantenlänge von 2000 Pixel mit 600dpi. Wie gross in cm wird dieses?

2000 / 600 * 2.54 = 8.46 cm

### 7) Berechnen sie den Speicherbedarf für ein unkomprimiertes Einzelbild im HD1080p50-Format bei einer True-Color-Farbauflösung.

1920 * 1080 * 24 / 8 = 6'220'800 bytes oder 6.22 MB

### 8) Welchen Speicherbedarf aus einer HD (Massvorsatz im IEC-Format) hat das Video aus der vorangegangenen Aufgabe bei einer Spieldauer von 3 Minuten?

6'220'800 * 50 = 311'040'000 bytes

311'040'000 * 60 * 3 = 55'987'200'000 bytes oder 55.99 GiB

### 9) Ihre Digitalkamera bietet für die Speicherung ihrer Bilder die beiden Formate RAW und JPG an. Wo liegen die Unterschiede und was sind die Verwendungszwecke?.

RAW Format sind die unkomprimierte Pixel Daten von einem Sensor. Ein JPG ist die komprimierte Pixel Daten von einem Sensor. Mit einem RAW Bild, kann man die Bilder viel einfacher bearbeiten und für extreme bearbeitung nutzlich. Weil ein JPG die Daten komprimiert, kann man nicht so extrem bearbeiten, aber es ist sehr speichereffizient (12MB zu 51MB Raw) und es wird von viele Plattformen unterschtütz und kann mit anderen einfach teilen.

### 10) Sie möchten ihr neulich erstelltes Gameplay-Video auf Youtube veröffentlichen. Was sind die technischen Vorgaben dazu? (Format, Bildrate, Farbauflösung, Video-, Audiocodec etc.). Gibt es allenfalls rechtliche Einschränkungen?

![Quelle](https://support.google.com/youtube/answer/4603579?hl=en)

Original HP1080P in MPEG-2 container, wenn nicht MPEG-2 möglich, dann ist MPEG-4 möglich

MPEG-2: Audio codec: MPEG Layer II oder Dolby AC-3, Bitrate: 128kbps oder besser

MPEG-4: Video codec: H.264, Audio Codec: AAC, bitrate: 128kbps oder besser

native framerate nutzen (resampling nicht empfohlen)

native aspect-ratio: beispiele: 4:3 oder 16:9

höchste qualität, wenn möglich

Dateiformate

MP3 Audio in MP3 / WAV container
PCM Audio in WAV container
AAC Audio in MOV container
FLAC Audio

minimut audio bitrate für lossy formate: 64kbps

### 11) Was ist der Unterschied zwischen dem Interlaced Mode und dem Progressive Mode?

Progressive Mode nimmt für jedes Bild ein ganzes Bild als Frame. Interlaced Mode werden die Bilder halbierd und als frame angezeigt. Interlaced braucht weniger bandbreite benutzt, aber bei schnellen bewegten bilder gibt es unerwartete kameraeffekte im Video

### 12) Was versteht man unter **Artefakten** und welche kennen sie?

Artefakte sind Daten, die unwerwartet Daten als Fehler von einem Gerät oder Programm in Dateiformate gespeichert werden. Audio kann kratzig für paar sekunden werden oder ein Frame in einem Video hat schlechte pixel oder das ganze Frame wird fehloren.

### 13) Berechnen Sie die Datenrate in GigaBit per Second oder kurz Gbps für die Übertragung eines unkomprimierten digitalen Videosignals HD1080i50 ohne Unterabtastung und 8 Bit Auflösung pro Farbkanal.

1080 * 1920 * 24 = 49'766'400 bits

49'766'400 * 60 = 2'985'984'000 bits

2.985 Gbps

### 14) Nach wie vielen Minuten unkomprimierten HD1080i50 Video wäre eine DVD-5 (Single-Layer DVD mit 4.7GB) voll?

1s = 373'248'000 bytes

4.7GB / 373'248'000 = 12.59s

### 15) Was ist der Unterschied zwischen einem Codec und einem Mediencontainer?

Ein Codec beschreibt, wie Daten werden encodiert und decodiert. diese codierung kann die Daten Komprimieren und / oder Datenverluste für Speicherplatz oder Qualität. Ein Mediencontainer ist ein behälter, dass mehrere codierungen enthalten kann und beschreibt, wie die Daten sollten gelesen werden.

### 16)

#### A) Warum benötigt man AD-Wandler?

Es wird ein AD-Wandler benötigt, wenn man ein Analog Signal (z.B. ein Temperatur Sensor oder microphone) zu einem Digitalen Signal umwandeln möchte. Viele Computer können nur Digitale Signale (0 und 1) speichern. Es vereinfacht die verteilung von Daten im Digital, anstelle von Analog.

#### B) Warum geht eine A/D-Wandlung immer mit einem Datenverlust einher?

Wenn man ein Analog signal umwandeln möchte, muss man nach par millisecunden die Welle Messen. die Zeitdauer, nachdem man messt ist die sogennante "Samplerate". Da es aber nicht Perfekt ist, da es nicht überall messen kann, werden nicht alle Daten mitbezogen und Daten gehen verloren.

#### C) Gibt eine höhere oder eine tiefer Samplingrate eine präzisere Abbildung des Originals? Begründen sie!

Eine höhere Samplingrate gibt eine präzisere Abblidung, da es messt pro sekunde viel mehr punkte als eine tiefere Samplingrate.

### 17)

### 18)

## Aufgaben zu Bilder komprimieren

### 1)

- rgb(255, 255, 255) in YC<sub>b</sub>C<sub>r</sub>: 1, 0, 0
- rgb(0, 0, 0) in YC<sub>b</sub>C<sub>r</sub>: 0, 0, 0
- YC<sub>b</sub>C<sub>r</sub>(0, 0.5, 0) ist die Farbe: rot
- YC<sub>b</sub>C<sub>r</sub>(0, -0.5, 0) ist die Farbe: grün
- YC<sub>b</sub>C<sub>r</sub>(0, 0, 0.5) ist die Farbe: blau
- YC<sub>b</sub>C<sub>r</sub>(0, 0, -0.5) ist die Farbe: dunkel grün
- YC<sub>b</sub>C<sub>r</sub>(0.3, 0.5, -0.17) ist die Farbe: rot

### 2) Das Bild soll in ein Graustufenbild umgewandelt werden. Berechnen sie den für das Hellblau entsprechende Grauwert.

Pixel wert für Hellblau: rgb(33, 121, 239)

33 * 0.3 + 121 * 0.6 + 239 * 0.1 = 106.4 => 106 Graustufenwert

### 3) Berechnen sie, wieviel Speicher eingespart wird, wenn ein Bild mit Subsampling 4:1:1 komprimiert wird.

Das Bild wird um hälfte vom Original Speicherbedarf gesinkt

### 4)

#### A) Kann man durch die Bildumwandlung vom RGB- in den YCbCr-Farbraum Speicherplatz einsparen?

Nein, denn es werden immer noch drei Farbenkanale benutzt.

#### B) Kann ein Beamer ein Bild im YC<sub>b</sub>C<sub>r</sub>-Farbbereich darstellen?

Nein, da ein Beamer benutzt additive Farbsystem, darum braucht es RGB

#### C) Wie rechnet man ein Farbbildes in ein Graustufenbild um?

rot * 0.3 + grün * 0.6 + blau * 0.1 = Graustufenwert

30% Rot, 60% grün und 10% blau

#### D) Warum hat bei der Umwandlung eine Farbbildes in ein Graustufenbild der Grünanteil am meisten Gewicht?

Da unser Auge sieht viel mehr verschiedenen Grün Farben als Blau farben

### 5) 

#### A) Warum verschlechtert sich die Bildschärfe von 4:1:1-Subsampling gegenüber 4:4:4-Subsampling nicht?

Weil das Luminanz bild bei beidenen Subsampling ist vorhanden, darum wird die schärfe gleich, aber die Farben können sich wenig verändern, da wir 1/4 der Farben in 4:1:1 verlieren.

#### B) Ein quadratisches 24-Bit-RGB-Bild mit einer Kantenlänge von 1000 Pixel soll mit 4:1:1 unterabgetatstet werden. Wieviel Speicherplatz wird damit eingespart?

1 pixel = 24 bit

1000 pixel = 24000 bit oder 3000 bytes oder 3 mB

24000 bit / 2 => 12000 bit oder 1500 bytes oder 1.5 mB

also um die hälfte

### 6)

#### A) Was ist der erste Schritt bei der JPG-Komprimierung?

Der Erste Schritt ist die umwandlung von RGB Farben zu YC<sub>b</sub>C<sub>r</sub> Farbbereich

#### B) Führt die DCT-Transformation zu einer Datenreduktion?

Nein, denn es umwandelt die Daten zu einer reduzierte, vereinfachten Version, wo später kann reduziert werden

#### C) Warum erhält man bei einer sehr starken Bildkomprimierung sogenannte Block-Artefakte?$

weil es reduziert die varianten, dass ein Block kann sein und es werden gleich-ähnliche blöcke zu einem umgewandelt in der DCT-Transformation.

### 7)

#### A) Was ist der Unterschied zwischen Intraframe- und Interframe-Komprimierung?

Intraframe komprimiert jede einzelen frame in einem Video. Interframe komprimiert zwischen frames und speichert nur veränderte frames und komprimiert sie. Interframe braucht kleiner Speicherbedarf als Intraframe, aber ist weniger qualität als Intraframe.

#### B) Bei welcher Filmsequenz bietet die Interframekomprimierung mehr Potential zur Datenreduzierung:

- I) 30 Sekunden-Szene mit Faultier auf Nahrungssuche?
- II) 30 Sekunden-Szene mit Zieleinfahrt beim Formel-1-Rennen?

Das erste hat mehr potential, denn es gibt sehr wenig bewegung und kann sehr gut komprimiert werden

#### C) Sehen sie Parallelen zwischen Datenbackupkonzepten und Interframe-Komprimierung?

bei den Incrementalen Datenbackup hat es parallene zu Interframe-Komprimierung

#### D) Was versteht man unter GOP25?

jedes 25. Bild ist ein komplettes bild für Interframe Komprimierung
